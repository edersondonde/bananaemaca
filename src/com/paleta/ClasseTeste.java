//**********************************************************************
//BANANA
//**********************************************************************
package com.paleta;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author fsakiyama
 *
 */
@Banana("adasdasd")
@Maca
public class ClasseTeste {

	public static final String EXT_XML = "xml";

	public static final String DIR_NAME_TC_LIB = "testCaseLib";

	private void listFilesAndFilesSubDirectories(String directoryName, List<File> projectFiles) {
		File directory = new File(directoryName);

		// get all the files from a directory
		File[] fList = directory.listFiles();

		for (File file : fList) {
			if (file.isFile()) {
				if (file.getName().endsWith(EXT_XML)) {
					projectFiles.add(file);
				}
			} else if (file.isDirectory()) {
				String directoryTempName = file.getName();
				if (!directoryTempName.contentEquals(DIR_NAME_TC_LIB)) {
					listFilesAndFilesSubDirectories(file.getPath(), projectFiles);
				}
			}
		}
	}

	/**
	 * Scan all the files from given path and return as a list.
	 */
	public List<File> scanProject(String path) {
		List<File> projectFiles = new ArrayList<File>();
		listFilesAndFilesSubDirectories(path, projectFiles);

		return projectFiles;
	}
}
