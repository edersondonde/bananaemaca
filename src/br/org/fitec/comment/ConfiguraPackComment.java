// **********************************************************************
// BANANA
// **********************************************************************
package br.org.fitec.comment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * ta ficando um monstro saporra
 * 
 * @author TIME RM
 *
 */
public class ConfiguraPackComment {

	private static Logger logger = LoggerFactory.getLogger(ConfiguraPackComment.class);

	public static void listf(String directoryName, ArrayList<File> files) {
		File directory = new File(directoryName);

		File[] fList = directory.listFiles();
		for (File file : fList) {
			if (file.isFile()) {
				if (file.getName().endsWith(".java")) {
					files.add(file);
				}
			} else if (file.isDirectory()) {
				listf(file.getAbsolutePath(), files);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		if (args.length == 0) {
			args = new String[1];
			// args[0] =
			// "C:\\cygwin64\\home\\zsakfab\\bananaemaca\\src\\com\\paleta\\";
			args[0] = "C:\\cygwin64\\home\\zsakfab\\bssf_finance\\master\\finance\\";
			// throw new IllegalArgumentException("It is expected a path as
			// argument");
		}
		String path = args[0];

		BufferedReader in = new BufferedReader(new InputStreamReader(
				ConfiguraPackComment.class.getResourceAsStream("/modeloCopyRight"), Charset.forName("UTF-8")));
		StringBuilder sb = new StringBuilder();
		String str;
		while ((str = in.readLine()) != null) {
			sb.append(str);
			sb.append("\n");
		}
		in.close();
		String modeloCopyRightFromTxt = sb.toString();

		ArrayList<File> files = new ArrayList<>();
		listf(path, files);

		for (File file : files) {
			BufferedReader br = new BufferedReader(new FileReader(file));

			boolean achouInicioClasse = false;
			List<String> comments = new ArrayList<>();
			List<String> lines = new ArrayList<>();
			String packageDeclaration = null;
			String importDeclaration = "";
			String line;
			boolean achouCodigo = false;
			boolean alteraClasse = true;
			String linhaAcima = "";
			while ((line = br.readLine()) != null) {

				if (line.startsWith("//") && !achouCodigo) {
					alteraClasse = false;
					break;
				}

				if (isBlankLineBetweenImports(linhaAcima, line)) {
					// Keep blank line between imports
					importDeclaration += line + "\n";
				}

				if (isImport(line)) {
					importDeclaration += line + "\n";
				}

				boolean isPackageDeclaration = line.startsWith("package");
				if (isPackageDeclaration) {
					achouCodigo = true;
					packageDeclaration = line + "\n";
				}

				if (isPublic(line) || isClass(line) || isJavadoc(line) || isAnnotation(line)) {
					achouCodigo = true;
					achouInicioClasse = true;
				}

				if (!achouInicioClasse && !isPackageDeclaration && !isImport(line)) {
					comments.add(line);
				}

				if (achouInicioClasse) {
					lines.add(line);
				}
				linhaAcima = line;
			}
			br.close();

			if (alteraClasse) {
				logger.info("Fixing copyright of file {}", file.getAbsolutePath());
				File tempFile = new File(file.toPath() + "_temp");
				PrintWriter pw = new PrintWriter(new FileWriter(tempFile));

				achouInicioClasse = false;
				StringBuilder sb2 = new StringBuilder();
				for (String comment : comments) {
					sb2.append(comment + "\n");
				}
				String trim = sb2.toString().trim();
				if (trim.isEmpty()) {
					pw.print(modeloCopyRightFromTxt.trim());
				} else {
					pw.print(trim);

				}
				pw.print("\n" + packageDeclaration + "\n");
				pw.print(importDeclaration);
				for (String string : lines) {
					pw.println(string);
				}
				pw.flush();
				pw.close();

				// Delete the original file
				if (!file.delete()) {
					System.out.println("Could not delete file");
					return;
				}

				// Rename the new file to the filename the original file had.
				if (!tempFile.renameTo(file)) {
					System.out.println("Could not rename file");
				}
			}

		}
	}

	private static boolean isAnnotation(String line) {
		return line.startsWith("@");
	}

	private static boolean isBlankLineBetweenImports(String linhaAcima, String line) {
		return isImport(linhaAcima) && line.equals("");
	}

	private static boolean isClass(String line) {
		return line.startsWith("class");
	}

	private static boolean isPublic(String line) {
		return line.startsWith("public");
	}

	private static boolean isImport(String line) {
		return line.startsWith("import");
	}

	private static boolean isJavadoc(String line) {
		return line.startsWith("/**");
	}

}
